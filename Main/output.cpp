#include "output.h"
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

void output(comm* array) {

	cout << array->number << " ";
	cout << array->date.day << ".";
	cout << array->date.month << ".";
	cout << array->date.year << " ";
	cout << array->start.hour << ".";
	cout << array->start.min << ".";
	cout << array->start.sec << " ";
	cout << array->last.hour << ".";
	cout << array->last.min << ".";
	cout << array->last.sec << " ";
	cout << array->type << " ";
	cout << array->value << " ";

	cout << '\n';

}