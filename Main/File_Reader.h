#ifndef FILE_READER_H
#define FILE_READER_H

#include "comm.h"
#include "constans.h"

void read(const char* file_name, comm* array[], int& size);

#endif
