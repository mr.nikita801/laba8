#include "constans.h"
#include "comm.h"
#include "File_Reader.h"
#include "filter.h"
#include "output.h"
#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #9. Communication\n";
    cout << "Author: Nikita Penchyk\n";
   


    comm* array[MAX_FILE_ROWS_COUNT];
    int size;

    try 
    {

        read("data.txt", array, size);

        for (int i = 0; i < size; i++)
        {
            output(array[i]);
        }

        int k;
        cout << "�������� ��� ������: ";
        cin >> k;
        switch (k)
        {
        case 1: typefilt(array, size);
            break;
        case 2: monthfilt(array, size);
            break;
        }
        for (int i = 0; i < size; i++)
        {
            delete array[i];
        }
    }
    catch (const char* error) {
        cout << error << '\n';
    }
    
    return 0;
}
