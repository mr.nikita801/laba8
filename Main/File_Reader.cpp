#include <fstream>
#include <cstring>
#include <string>
#include <iostream>

#include "File_Reader.h"

using namespace std;

date convertDate(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.year = atoi(str_number);
    return result;
}

times convertTime(char* str)
{
    times result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.min = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.sec = atoi(str_number);
    return result;
}



void read(const char* file_name, comm* array[], int& size) {

    std::ifstream file(file_name);
    if (file.is_open())
    {
        int i = 0;
        char tmp_buffer[MAX_STRING_SIZE];


        while (!file.eof()) {

            comm* item = new comm;

            file >> item->number;
            file >> tmp_buffer;
            item->date = convertDate(tmp_buffer);
            file >> tmp_buffer;
            item->start = convertTime(tmp_buffer);
            file >> tmp_buffer;
            item->last = convertTime(tmp_buffer);
            file >> item->type;
            file.ignore();
            file >> item->value;

            array[i] = item;
            i++;
        }
        size = i;
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}